import React from 'react'
import ReactDom from 'react-dom'
import TodoContainer from './TodoContainer.js'

ReactDom.render(<TodoContainer/>, document.getElementById('todoContainer'))